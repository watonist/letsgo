# Application bootstrap

Example use

```go
package main

import (
	"gitlab.com/watonist/letsgo/bootstrap"
)

func main() {
	defer bootstrap.GracefulShutdown()

    // actual work goes here
}
```

```go
package main

import (
    "fmt"

	"gitlab.com/watonist/letsgo/bootstrap"
)

func init() {
	bootstrap.AddShutdownRoutine(func() {
		fmt.Println("Execute at shutdown")
	})
}

func main() {
	defer bootstrap.GracefulShutdown()

    // actual work goes here
}
```

```go
package main

import (
	"gitlab.com/watonist/letsgo/bootstrap"
)

func main() {
	defer bootstrap.GracefulShutdown()

    // goroutine service
    go func() {
        // actual service code
    }()

    // another goroutine service
    go func() {
        // actual service code
    }()

    // wait for application termination
    bootstrap.WaitForTermination()
}
```

---
[Back](../README.md)
