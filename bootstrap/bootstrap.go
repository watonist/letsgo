package bootstrap

import (
	"os"
	"os/signal"
	"sync"
	"sync/atomic"
	"syscall"

	"github.com/sirupsen/logrus"
	"gitlab.com/watonist/letsgo/log"
)

var shutdownSync sync.WaitGroup
var explicitExit int32 = 0
var exitStatus int32 = 0
var doSystemExit int32 = 0

func init() {
	shutdownSync.Add(1)

	// handling OS signal
	var osSignal = make(chan os.Signal, 1)
	signal.Notify(osSignal, syscall.SIGTERM, syscall.SIGINT, syscall.SIGQUIT)

	go func() {
		sig := <-osSignal
		signal.Stop(osSignal)

		log.Logger().WithField("signal", sig).Info("OS signal caught")
		internalShutdown("OS termination signal", nil)

		// immediate exit when no other process waiting
		if atomic.LoadInt32(&explicitExit) == 0 {
			i := atomic.LoadInt32(&exitStatus)
			log.Logger().WithField("exit-status", i).Info("System exit")
			os.Exit(int(i))
		}
	}()

	logrus.RegisterExitHandler(gracefulShutdownLogger)
}

// WaitForTermination wait for process termination
func WaitForTermination() {
	atomic.AddInt32(&explicitExit, 1)
	shutdownSync.Wait()
	if atomic.CompareAndSwapInt32(&doSystemExit, 0, 1) {
		if i := atomic.LoadInt32(&exitStatus); i != 0 {
			log.Logger().WithField("exit-status", i).Error("System exit")
			os.Exit(int(i))
		}
	}
}
