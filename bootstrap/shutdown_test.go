package bootstrap_test

import (
	"fmt"

	"gitlab.com/watonist/letsgo/bootstrap"
	"gitlab.com/watonist/letsgo/config"
	logModule "gitlab.com/watonist/letsgo/log"
	logConfig "gitlab.com/watonist/letsgo/log/config"
)

func init() {
	config.SetDefault(logConfig.LogConsoleEnable, true)
	config.SetDefault(logConfig.LogCallerEnable, true)
	logModule.PostInit()

	bootstrap.AddShutdownRoutine(func() {
		fmt.Println("1st stop")
	})
	bootstrap.AddShutdownRoutine(func() {
		fmt.Println("2nd stop")
	})
	bootstrap.AddShutdownRoutine(func() {
		fmt.Println("3rd stop")
	})
}

// func TestNormalShutdown(t *testing.T) {
// 	fmt.Println("TestNormalShutdown")
// 	defer bootstrap.GracefulShutdown()
// 	fmt.Println("Checkpoint")
// }

// func TestPanicShutdown(t *testing.T) {
// 	fmt.Println("TestPanicShutdown")
// 	defer bootstrap.GracefulShutdown()
// 	doPanic()
// 	fmt.Println("Unreachable")
// }

// func doPanic() {
// 	panic("Panic message")
// }
