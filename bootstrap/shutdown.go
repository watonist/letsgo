package bootstrap

import (
	"sync/atomic"

	"gitlab.com/watonist/letsgo/log"
)

// ShutdownRoutine function executed on shutdown process
type ShutdownRoutine struct {
	hasParams       bool
	normalFn        func()
	parameterizedFn func(any) error
	fnParam         any
}

var shutdownRoutines []ShutdownRoutine

// AddShutdownRoutine add module specific stop handler
func AddShutdownRoutine(fn func()) {
	shutdownRoutines = append(shutdownRoutines, ShutdownRoutine{
		hasParams: false,
		normalFn:  fn,
	})
}

// AddShutdownRoutineWithParam add module specific stop handler, with parameter
func AddShutdownRoutineWithParam(fn func(any) error, param any) {
	shutdownRoutines = append(shutdownRoutines, ShutdownRoutine{
		hasParams:       true,
		parameterizedFn: fn,
		fnParam:         param,
	})
}

// SafePanic recover panic status and perform graceful shutdown routines on abnormal exit
func SafePanic() {
	panicMsg := recover()
	if panicMsg != nil {
		internalShutdown("Program exit", panicMsg)
	}
}

// GracefulShutdown perform graceful shutdown routines
func GracefulShutdown() {
	panicMsg := recover()
	internalShutdown("Program exit", panicMsg)
}

// gracefulShutdownLogger perform graceful shutdown routines by logger fatal events
func gracefulShutdownLogger() {
	atomic.StoreInt32(&exitStatus, 1)
	internalShutdown("Log handler exit", nil)
}

var shutdownOps int32

func internalShutdown(topic string, panicMsg any) {

	var panicStatus bool
	if panicMsg == nil {
		panicStatus = false
	} else {
		atomic.StoreInt32(&exitStatus, 255)
		panicStatus = true
	}

	logger := log.Logger().WithField("topic", topic)
	if panicStatus {
		logger.WithField("panic", panicMsg).Debug("Executing internal shutdown routine(s) ...")
	} else {
		logger.Debug("Executing internal shutdown routine(s) ...")
	}

	// prevent race condition & double execution of shutdown process
	if !atomic.CompareAndSwapInt32(&shutdownOps, 0, 1) {
		return
	}

	if panicStatus {
		logger.WithField("panic", panicMsg).Errorln("Executing shutdown routine(s) ...")
	} else {
		logger.Info("Executing shutdown routine(s) ...")
	}

	// execute registered shutdown routines, start from latest one (LIFO)
	for i := len(shutdownRoutines) - 1; i >= 0; i-- {
		fStop := shutdownRoutines[i]
		if fStop.hasParams {
			if err := fStop.parameterizedFn(fStop.fnParam); err != nil {
				logger.WithError(err).Error("Shutdown routine error")
			}
		} else {
			fStop.normalFn()
		}
	}
	shutdownSync.Done()
}
