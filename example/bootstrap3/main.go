package main

import (
	"fmt"
	"time"

	"gitlab.com/watonist/letsgo/bootstrap"
	"gitlab.com/watonist/letsgo/config"
	"gitlab.com/watonist/letsgo/log"
	logCfg "gitlab.com/watonist/letsgo/log/config"
)

func main() {
	defer bootstrap.GracefulShutdown()

	config.SetDefault(logCfg.LogConsoleEnable, true)
	config.SetDefault(logCfg.LogFileEnable, false)
	config.SetDefault(logCfg.LogLevel, "debug")
	log.PostInit()

	doneSignal := make(chan bool)
	bootstrap.AddShutdownRoutine(func() {
		fmt.Println("Closing done signal")
		close(doneSignal)
	})

	go func() {
		defer bootstrap.SafePanic()

		ticker := time.NewTicker(time.Second * 1)
		bootstrap.AddShutdownRoutine(func() {
			fmt.Println("Stopping service 1")
			ticker.Stop()
		})

		for {
			var tm time.Time

			fmt.Println("Service 1 select")
			select {
			case _, ok := <-doneSignal:
				if !ok {
					doneSignal = nil
				}
				fmt.Println("Done signal caught on service 1...")
				return
			case t, ok := <-ticker.C:
				if !ok {
					fmt.Println("Ticker channel invalid")
					ticker.Stop()
					continue
				}
				tm = t
			}

			fmt.Println("Service 1:", tm)
		}
	}()

	go func() {
		defer bootstrap.SafePanic()

		ticker := time.NewTicker(time.Second * 1)
		bootstrap.AddShutdownRoutine(func() {
			fmt.Println("Stopping service 2")
			ticker.Stop()
		})

		for {
			var tm time.Time

			fmt.Println("Service 2 select")
			select {
			case _, ok := <-doneSignal:
				if !ok {
					doneSignal = nil
				}
				fmt.Println("Done signal caught on service 2...")
				return
			case t, ok := <-ticker.C:
				if !ok {
					fmt.Println("Ticker channel invalid")
					ticker.Stop()
					continue
				}
				tm = t
			}

			fmt.Println("Service 2:", tm)
		}
	}()

	bootstrap.WaitForTermination()
}
