package main

import (
	"fmt"
	"time"

	"gitlab.com/watonist/letsgo/bootstrap"
	"gitlab.com/watonist/letsgo/config"
	"gitlab.com/watonist/letsgo/log"
	logConfig "gitlab.com/watonist/letsgo/log/config"
)

func init() {
	bootstrap.AddShutdownRoutine(func() {
		fmt.Println("Shutting down main process")
	})
}

func main() {
	defer bootstrap.GracefulShutdown()

	config.SetDefault(logConfig.LogConsoleEnable, true)
	config.SetDefault(logConfig.LogFileEnable, false)
	config.SetDefault(logConfig.LogLevel, "debug")
	log.PostInit()
	time.Sleep(time.Second * 1)

	fmt.Println("Do actual work")
}
