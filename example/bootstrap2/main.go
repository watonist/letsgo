package main

import (
	"fmt"
	"time"

	"gitlab.com/watonist/letsgo/bootstrap"
	"gitlab.com/watonist/letsgo/config"
	"gitlab.com/watonist/letsgo/log"
	logConfig "gitlab.com/watonist/letsgo/log/config"
)

func init() {
	bootstrap.AddShutdownRoutine(func() {
		fmt.Println("Shutting down main process")
		time.Sleep(time.Second * 1)
		fmt.Println("Main process stopped")
	})
}

func main() {
	defer func() {
		fmt.Println("Defer main process")
		bootstrap.GracefulShutdown()
	}()

	config.SetDefault(logConfig.LogConsoleEnable, true)
	config.SetDefault(logConfig.LogFileEnable, false)
	config.SetDefault(logConfig.LogLevel, "debug")
	log.PostInit()
	time.Sleep(time.Second * 1)

	go func() {
		defer bootstrap.SafePanic()

		for {
			fmt.Println("Service 1")
			time.Sleep(time.Second * 1)
		}
	}()

	go func() {
		defer bootstrap.SafePanic()

		for {
			fmt.Println("Service 2")
			time.Sleep(time.Second * 2)
		}
	}()

	go func() {
		defer bootstrap.SafePanic()

		time.Sleep(time.Second * 10)
		fmt.Println("Exiting service 3")
	}()

	go func() {
		defer bootstrap.SafePanic()

		time.Sleep(time.Second * 15)
		panic("Force timeout on service 4")
	}()

	bootstrap.WaitForTermination()
}
