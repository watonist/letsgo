# letsgo

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/watonist/letsgo)](https://goreportcard.com/report/gitlab.com/watonist/letsgo)
[![Pipeline](https://gitlab.com/watonist/letsgo/badges/master/pipeline.svg)](./)

Simple go application bootstrap

## Components

* [bootstrap](bootstrap/README.md)
* [logger](log/README.md)
* [config](config/README.md)

## Build & testing

### Linter

```bash
docker run --rm -v $(pwd):/app -w /app golangci/golangci-lint:v1.46.2 golangci-lint run -v
```
