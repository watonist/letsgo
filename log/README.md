# Logger

[Logrus](https://github.com/sirupsen/logrus) based logging.

Configuration example:

```yaml
log:
  enable: true
  # log level: trace, debug, info, warn, error, fatal
  level: debug
  # log format: TEXT or JSON
  format: text
  caller:
    enable: false
  console:
    enable: true
    # output to stdout or stderr
    stream: stderr
  file:
    enable: false
    path: logs/application.log
    compress: true
    limit:
      # max file size, in megabytes
      size: 50
      # max file backup
      backup: 10
      # max file age, in days
      age: 30
```

here is logging initializer example, you can put it anywhere at the package

```go
import (
	"github.com/sirupsen/logrus"
	"gitlab.com/watonist/letsgo/log"
	logkey "gitlab.com/watonist/letsgo/log/key"
)

func logFn(fn string) log.Fn {
	return log.LazyLogger(func() *logrus.Entry {
		return log.Logger().WithField(logkey.Module, "client/"+fn)
	})
}

var logger = log.LazyLogger(func() *logrus.Entry {
	return log.Logger().WithField(logkey.Module, "client")
})
```

and then try to log something out

```go
    logger().
		WithField("status", resp.Status).
		WithField("message", resp.Message).
		WithField("id", resp.Client).
		Debug("Registration response")
```

---
[Back](../README.md)
