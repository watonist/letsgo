package key

const (
	Scope    string = "scope"
	Stage    string = "stage"
	Module   string = "module"
	Command  string = "cmd"
	Function string = "fn"
)
