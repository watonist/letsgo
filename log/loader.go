package log

import (
	"io"
	"os"
	"path/filepath"
	"strings"

	nlf "github.com/antonfisher/nested-logrus-formatter"
	joonix "github.com/joonix/log"
	"github.com/kardianos/osext"
	cjf "github.com/nolleh/caption_json_formatter"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	logConfig "gitlab.com/watonist/letsgo/log/config"
	logkey "gitlab.com/watonist/letsgo/log/key"
	lumberjack "gopkg.in/natefinch/lumberjack.v2"
)

func initLogger() {
	enableLogPid = viper.GetBool(logConfig.LogProcessID)

	initVerbosity()
	initFormatter()
	initWriter()
}

func initVerbosity() {
	if !viper.GetBool(logConfig.LogEnable) {
		logrus.SetReportCaller(false)
		logrus.SetLevel(logrus.PanicLevel)
		return
	}

	logrus.SetReportCaller(viper.GetBool(logConfig.LogCallerEnable))

	switch strings.ToUpper(viper.GetString(logConfig.LogLevel)) {
	case "TRACE":
		logrus.SetLevel(logrus.TraceLevel)
	case "DEBUG":
		logrus.SetLevel(logrus.DebugLevel)
	case "INFO":
		logrus.SetLevel(logrus.InfoLevel)
	case "WARN", "WARNING":
		logrus.SetLevel(logrus.WarnLevel)
	case "ERROR":
		logrus.SetLevel(logrus.ErrorLevel)
	case "FATAL":
		logrus.SetLevel(logrus.FatalLevel)
	default:
		logrus.SetLevel(logrus.InfoLevel)
	}
}

func initFormatter() {
	switch logConfig.Formatter() {
	case "json":
		logrus.SetFormatter(&logrus.JSONFormatter{})
	case "caption-json", "caption_json", "captionjson":
		logrus.SetFormatter(&cjf.Formatter{PrettyPrint: true})
	case "nested":
		logrus.SetFormatter(&nlf.Formatter{
			HideKeys:    true,
			FieldsOrder: []string{logkey.Module, logkey.Scope, logkey.Stage},
		})
	case "fluentd":
		logrus.SetFormatter(joonix.NewFormatter())
	case "text", "txt":
		logrus.SetFormatter(&logrus.TextFormatter{
			FullTimestamp:    true,
			DisableTimestamp: false,
		})
	default:
		logrus.SetFormatter(&logrus.TextFormatter{
			FullTimestamp:    true,
			DisableTimestamp: false,
		})
	}
}

func initWriter() {
	if !viper.GetBool(logConfig.LogEnable) {
		logrus.SetOutput(io.Discard)
		return
	}

	toConsole := viper.GetBool(logConfig.LogConsoleEnable)
	toFile := viper.GetBool(logConfig.LogFileEnable)

	if toConsole && toFile {
		consoleOut := os.Stdout
		if logConfig.ToStderr() {
			consoleOut = os.Stderr
		}
		multiWriter := io.MultiWriter(consoleOut, &lumberjack.Logger{
			Filename:   getLogFile(),
			MaxSize:    viper.GetInt(logConfig.LogFileLimitSize), // megabytes
			MaxBackups: viper.GetInt(logConfig.LogFileLimitBackup),
			MaxAge:     viper.GetInt(logConfig.LogFileLimitAge), //days
			LocalTime:  viper.GetBool(logConfig.LogFileLocaltime),
			Compress:   viper.GetBool(logConfig.LogFileCompress),
		})
		logrus.SetOutput(multiWriter)
		return
	}

	if toConsole {
		if logConfig.ToStderr() {
			logrus.SetOutput(os.Stderr)
		} else {
			logrus.SetOutput(os.Stdout)
		}
		return
	}

	if toFile {
		logrus.SetOutput(&lumberjack.Logger{
			Filename:   getLogFile(),
			MaxSize:    viper.GetInt(logConfig.LogFileLimitSize), // megabytes
			MaxBackups: viper.GetInt(logConfig.LogFileLimitBackup),
			MaxAge:     viper.GetInt(logConfig.LogFileLimitAge), //days
			LocalTime:  viper.GetBool(logConfig.LogFileLocaltime),
			Compress:   viper.GetBool(logConfig.LogFileCompress),
		})
		return
	}

	logrus.SetOutput(io.Discard)
}

func getLogFile() string {
	logfile := viper.GetString(logConfig.LogFilePath)
	if filepath.IsAbs(logfile) {
		return logfile
	}

	dir, err := osext.ExecutableFolder()
	if err != nil {
		Logger().WithError(err).
			WithField(logkey.Scope, "logger").
			WithField(logkey.Stage, "init").
			Fatal("Unable to get application directory")
	}
	return filepath.Clean(filepath.Join(dir, logfile))
}
