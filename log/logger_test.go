package log_test

import (
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/watonist/letsgo/config"
	"gitlab.com/watonist/letsgo/log"
	logConfig "gitlab.com/watonist/letsgo/log/config"
)

var logMaster = log.LazyLogger(func() *logrus.Entry {
	return log.Logger().WithField("modules", "logtest")
})

var logFn1 = log.LazyLogger(func() *logrus.Entry {
	return logMaster().WithField("fn", "Fn1")
})

var logFn2 = log.LazyLogger(func() *logrus.Entry {
	return logMaster().WithField("fn", "Fn2")
})

func logFn(fn string) log.Fn {
	return log.LazyLogger(func() *logrus.Entry {
		return logMaster().WithField("fn", fn)
	})
}

func TestLog(t *testing.T) {
	config.SetDefault(logConfig.LogConsoleEnable, true)
	config.SetDefault(logConfig.LogLevel, "debug")
	log.PostInit()
	log.Logger().Infoln("Hello world")
	time.Sleep(time.Second * 1)
	log.Logger().Infoln("Message 1")

	// logMaster().Info("Test logMaster")
	// logFn1().Info("Test logFn1")
	logFn2().Info("Test logFn2")
}
