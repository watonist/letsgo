package config

import (
	"strings"

	"github.com/spf13/viper"
)

const (
	LogEnable    string = "log.enable"
	LogProcessID string = "log.pid"
	LogLevel     string = "log.level"

	LogFormat       string = "log.format"
	LogCallerEnable string = "log.caller.enable"

	LogConsoleEnable string = "log.console.enable"
	LogConsoleStream string = "log.console.stream"

	LogFileEnable      string = "log.file.enable"
	LogFilePath        string = "log.file.path"
	LogFileLocaltime   string = "log.file.localtime"
	LogFileCompress    string = "log.file.compress"
	LogFileLimitSize   string = "log.file.limit.size"
	LogFileLimitBackup string = "log.file.limit.backup"
	LogFileLimitAge    string = "log.file.limit.age"
)

func init() {
	viper.SetDefault(LogEnable, true)
	viper.SetDefault(LogProcessID, false)
	viper.SetDefault(LogLevel, "info")

	viper.SetDefault(LogFormat, "text")
	viper.SetDefault(LogCallerEnable, false)

	viper.SetDefault(LogConsoleEnable, false)
	viper.SetDefault(LogConsoleStream, "stderr")

	viper.SetDefault(LogFileEnable, false)
	viper.SetDefault(LogFilePath, "")
	viper.SetDefault(LogFileLocaltime, true)
	viper.SetDefault(LogFileCompress, false)
	viper.SetDefault(LogFileLimitSize, 50)
	viper.SetDefault(LogFileLimitBackup, 10)
	viper.SetDefault(LogFileLimitAge, 60)
}

func FormatterJSON() bool {
	return strings.ToLower(viper.GetString(LogFormat)) == "json"
}

func Formatter() string {
	return strings.TrimSpace(strings.ToLower(viper.GetString(LogFormat)))
}

func ToStderr() bool {
	return strings.ToLower(viper.GetString(LogConsoleStream)) == "stderr"
}

func ToStdout() bool {
	return strings.ToLower(viper.GetString(LogConsoleStream)) == "stdout"
}
