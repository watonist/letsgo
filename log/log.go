package log

import (
	"os"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

var enableLogPid = false

func init() {
	initLogger()
}

// PostInit initialize module after load configuration
func PostInit() {
	initLogger()
}

// Fn log function
type Fn func() *logrus.Entry

// Logger get logger instance
func Logger() *logrus.Entry {
	if enableLogPid {
		return logrus.WithFields(logrus.Fields{
			"pid":  os.Getpid(),
			"ppid": os.Getppid(),
		})
	}
	return logrus.NewEntry(logrus.StandardLogger())
}

// logSeed logger initializer
type logSeed struct {
	fn    Fn
	entry *logrus.Entry
	once  sync.Once
}

// LazyLogger lazy instance logger
func LazyLogger(fn Fn) Fn {
	return logSprout(&logSeed{fn: fn})
}

func logSprout(seed *logSeed) Fn {
	return func() *logrus.Entry {
		seed.once.Do(func() {
			seed.entry = seed.fn()
			seed.entry.Info("Init logger")
		})
		return seed.entry
	}
}

// ElapsedTime log process elapsed time
func ElapsedTime(logger *logrus.Entry, format string, a ...interface{}) func() {
	start := time.Now()
	return func() {
		logger.WithFields(logrus.Fields{
			"start":   start,
			"elapsed": time.Since(start),
		}).Infof(format, a...)
	}
}
