package config

import (
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/watonist/letsgo/log"
)

var logger = log.LazyLogger(func() *logrus.Entry {
	return log.Logger().WithField("module", "config")
})

type keyAliases []string

var key2alias map[string]keyAliases = make(map[string]keyAliases)
var alias2key map[string]string = make(map[string]string)

// SetAliases register key aliases without default value
func SetAliases(key string, aliases ...string) {
	if key == "" {
		return
	}
	aliasLen := len(aliases)
	localAliases := make(keyAliases, aliasLen)
	for i := 0; i < aliasLen; i++ {
		alias := aliases[i]
		if alias == "" {
			continue
		}
		alias2key[alias] = key
		localAliases[i] = alias
	}

	key2alias[key] = localAliases
	alias2key[key] = key
}

// SetDefault register key aliases and set default value
func SetDefault(key string, value interface{}, aliases ...string) {
	if key == "" {
		return
	}
	SetAliases(key, aliases...)
	viper.SetDefault(key, value)
}

func realKey(keyGuess string) string {
	key, keyExist := alias2key[keyGuess]
	if keyExist {
		return key
	}
	return keyGuess
}

func findKey(key string) (string, bool) {
	theRealKey := realKey(key)
	aliases, aliasExist := key2alias[theRealKey]
	if aliasExist {
		for _, alias := range aliases {
			if viper.IsSet(alias) {
				return alias, true
			}
		}
	}

	// realKey normally contains value from SetDefault() so it will be the last option
	return theRealKey, viper.IsSet(theRealKey)
}

var fatalHandler func(string) = func(key string) {
	logger().Fatalf("Undefined config '%s'", key)
}

// SetFatalHandler set handler function if config key not found
func SetFatalHandler(handler func(string)) {
	fatalHandler = handler
}

func doOrDie(keyExist bool, key string) {
	if !keyExist {
		fatalHandler(key)
	}
}

// InConfig checks to see if the key/aliases is in the config file.
func InConfig(key string) bool {
	theRealKey := realKey(key)
	aliases, aliasExist := key2alias[theRealKey]
	if aliasExist {
		for _, alias := range aliases {
			if viper.InConfig(alias) {
				return true
			}
		}
	}

	return viper.InConfig(theRealKey)
}

// IsSet checks to see if the key/aliases has been set
func IsSet(key string) bool {
	_, keyExist := findKey(key)
	return keyExist
}

func GetBool(key string) bool {
	validKey, _ := findKey(key)
	return viper.GetBool(validKey)
}

func MustGetBool(key string) bool {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetBool(validKey)
}

func GetDuration(key string) time.Duration {
	validKey, _ := findKey(key)
	return viper.GetDuration(validKey)
}

func MustGetDuration(key string) time.Duration {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetDuration(validKey)
}

func GetFloat64(key string) float64 {
	validKey, _ := findKey(key)
	return viper.GetFloat64(validKey)
}

func MustGetFloat64(key string) float64 {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetFloat64(validKey)
}

func GetInt(key string) int {
	validKey, _ := findKey(key)
	return viper.GetInt(validKey)
}

func MustGetInt(key string) int {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetInt(validKey)
}

func GetInt32(key string) int32 {
	validKey, _ := findKey(key)
	return viper.GetInt32(validKey)
}

func MustGetInt32(key string) int32 {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetInt32(validKey)
}

func GetInt64(key string) int64 {
	validKey, _ := findKey(key)
	return viper.GetInt64(validKey)
}

func MustGetInt64(key string) int64 {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetInt64(validKey)
}

func GetIntSlice(key string) []int {
	validKey, _ := findKey(key)
	return viper.GetIntSlice(validKey)
}

func MustGetIntSlice(key string) []int {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetIntSlice(validKey)
}

func GetSizeInBytes(key string) uint {
	validKey, _ := findKey(key)
	return viper.GetSizeInBytes(validKey)
}

func MustGetSizeInBytes(key string) uint {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetSizeInBytes(validKey)
}

func GetString(key string) string {
	validKey, _ := findKey(key)
	return viper.GetString(validKey)
}

func MustGetString(key string) string {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetString(validKey)
}

func GetStringMap(key string) map[string]interface{} {
	validKey, _ := findKey(key)
	return viper.GetStringMap(validKey)
}

func MustGetStringMap(key string) map[string]interface{} {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetStringMap(validKey)
}

func GetStringMapString(key string) map[string]string {
	validKey, _ := findKey(key)
	return viper.GetStringMapString(validKey)
}

func MustGetStringMapString(key string) map[string]string {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetStringMapString(validKey)
}

func GetStringMapStringSlice(key string) map[string][]string {
	validKey, _ := findKey(key)
	return viper.GetStringMapStringSlice(validKey)
}

func MustGetStringMapStringSlice(key string) map[string][]string {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetStringMapStringSlice(validKey)
}

func GetStringSlice(key string) []string {
	validKey, _ := findKey(key)
	return viper.GetStringSlice(validKey)
}

func MustGetStringSlice(key string) []string {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetStringSlice(validKey)
}

func GetTime(key string) time.Time {
	validKey, _ := findKey(key)
	return viper.GetTime(validKey)
}

func MustGetTime(key string) time.Time {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetTime(validKey)
}

func GetUint(key string) uint {
	validKey, _ := findKey(key)
	return viper.GetUint(validKey)
}

func MustGetUint(key string) uint {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetUint(validKey)
}

func GetUint32(key string) uint32 {
	validKey, _ := findKey(key)
	return viper.GetUint32(validKey)
}

func MustGetUint32(key string) uint32 {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetUint32(validKey)
}

func GetUint64(key string) uint64 {
	validKey, _ := findKey(key)
	return viper.GetUint64(validKey)
}

func MustGetUint64(key string) uint64 {
	validKey, keyExist := findKey(key)
	doOrDie(keyExist, key)
	return viper.GetUint64(validKey)
}
