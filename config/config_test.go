package config_test

import (
	"bytes"
	"testing"

	"github.com/spf13/viper"
	cfgUtil "gitlab.com/watonist/letsgo/config"
)

func expectBool(t *testing.T, key string, expected bool) {
	if result := cfgUtil.GetBool(key); result != expected {
		t.Errorf("Key:'%s' Result:%v Expected:%v\n", key, result, expected)
	}
}

func expectInt(t *testing.T, key string, expected int) {
	if result := cfgUtil.GetInt(key); result != expected {
		t.Errorf("Key:'%s' Result:%v Expected:%v\n", key, result, expected)
	}
}

func expectString(t *testing.T, key string, expected string) {
	if result := cfgUtil.GetString(key); result != expected {
		t.Errorf("Key:'%s' Result:%v Expected:%v\n", key, result, expected)
	}
}

func TestBoolValue(t *testing.T) {
	// Possible values
	// "1", "t", "T", "true", "TRUE", "True"
	// "0", "f", "F", "false", "FALSE", "False"
	rawConfig := []byte(`
vtest:
  v1: true
  v2: TRUE
  v3: T
  v4: False
  v5: YES
  v6: NO
`)
	viper.SetConfigType("YAML")
	viper.ReadConfig(bytes.NewBuffer(rawConfig))

	expectBool(t, "vtest.v0", false)
	cfgUtil.SetDefault("vtest.v0", true)

	expectBool(t, "vtest.v1", true)
	expectBool(t, "vtest.v2", true)
	expectBool(t, "vtest.v3", true)

	expectBool(t, "vtest.v4", false)
	expectBool(t, "vtest.v5", false)
	expectBool(t, "vtest.v6", false)

	expectBool(t, "vtest.v7", false)
}

func TestAliases(t *testing.T) {
	rawConfig := []byte(`
vtest:
  listen-host: 127.0.0.1
  listen:
    port: 8080
`)
	viper.SetConfigType("YAML")
	viper.ReadConfig(bytes.NewBuffer(rawConfig))

	cfgUtil.SetDefault("vtest.listen.host", "localhost", "vtest.listen-host", "vtest.listen_host")
	cfgUtil.SetDefault("vtest.listen.port", 80, "vtest.listen-port", "vtest.port")

	expectString(t, "vtest.listen.host", "127.0.0.1")
	expectString(t, "vtest.listen-host", "127.0.0.1")
	expectString(t, "vtest.listen_host", "127.0.0.1")

	expectInt(t, "vtest.listen.port", 8080)
	expectInt(t, "vtest.listen-port", 8080)
	expectInt(t, "vtest.port", 8080)
}

func TestMultiConfigSrc(t *testing.T) {
	rawConfig1 := []byte(`
vtest1:
  id: 150
`)
	viper.SetConfigType("YAML")
	viper.ReadConfig(bytes.NewBuffer(rawConfig1))
	rawConfig2 := []byte(`
vtest2:
  id: 350
`)
	viper.MergeConfig(bytes.NewBuffer(rawConfig2))

	expectInt(t, "vtest1.id", 150)
	expectInt(t, "vtest2.id", 350)
}
