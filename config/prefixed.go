package config

import (
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

type prefixAliases []string

var prefixMap map[string]*basePrefix = make(map[string]*basePrefix)
var prefix2alias map[string]prefixAliases = make(map[string]prefixAliases)
var alias2prefix map[string]string = make(map[string]string)

type basePrefix struct {
	prefix string
}

// Prefix configuration prefix
type Prefix struct {
	prefix string
	base   *basePrefix
}

func realPrefix(prefixes ...string) string {
	prefixLen := len(prefixes)
	if prefixLen == 0 {
		return ""
	}
	for _, prefix := range prefixes {
		if prefix == "" {
			continue
		}
		prefix, prefixExist := alias2prefix[prefix]
		if prefixExist {
			return prefix
		}
	}
	return prefixes[0]
}

// NewPrefix create prefixed configuration
func NewPrefix(prefix string) (*Prefix, error) {
	realPrefix := realPrefix(prefix)
	if realPrefix == "" {
		return nil, errors.New("Empty prefix")
	}

	resultBase, prefixExist := prefixMap[realPrefix]
	if !prefixExist {
		resultBase = &basePrefix{
			prefix: realPrefix,
		}
		prefixMap[realPrefix] = resultBase
	}
	if err := resultBase.SetPrefixAlias(realPrefix); err != nil {
		return nil, errors.Wrapf(err, "Fail to set prefix alias")
	}
	return &Prefix{prefix: prefix, base: resultBase}, nil
}

func (c *Prefix) SetPrefixAlias(aliases ...string) error {
	return c.base.SetPrefixAlias(aliases...)
}

func (c *basePrefix) SetPrefixAlias(aliases ...string) error {
	localAliases := make(prefixAliases, 0)

	currentAliases, aliasesExist := prefix2alias[c.prefix]
	if !aliasesExist {
		currentAliases = make(prefixAliases, 0)
	}
	localAliases = append(localAliases, currentAliases...)

	for i := 0; i < len(aliases); i++ {
		alias := aliases[i]
		if alias == "" {
			continue
		}
		if alias == c.prefix {
			continue
		}

		prefixCheck, prefixExist := alias2prefix[alias]
		if prefixExist {
			if prefixCheck != c.prefix {
				return errors.Errorf("Prefix '%s' already aliased to '%s'", alias, prefixCheck)
			}
		}

		alias2prefix[alias] = c.prefix
		localAliases = append(localAliases, alias)
	}
	prefix2alias[c.prefix] = localAliases
	alias2prefix[c.prefix] = c.prefix
	return nil
}

func (c *Prefix) key(key string) string {
	if strings.HasSuffix(c.prefix, ".") {
		return c.prefix + key
	}
	return c.prefix + "." + key
}

func (c *Prefix) GetPrefix() string {
	return c.prefix
}

func (c *Prefix) GetPrefixAliases() []string {
	prefix, prefixExist := prefix2alias[c.base.prefix]
	if !prefixExist {
		return make([]string, 0)
	}
	return prefix
}

func (c *Prefix) findKey(key string) (string, bool) {
	theRealKey := realKey(key)
	keyAliases, aliasExist := key2alias[theRealKey]

	// 1st processing direct prefix
	prefixHasDotEnd := strings.HasPrefix(c.prefix, ".")
	if aliasExist {
		for _, alias := range keyAliases {
			if prefixedKey := c.prefix + alias; viper.IsSet(prefixedKey) {
				return prefixedKey, true
			}
			if !prefixHasDotEnd {
				if prefixedKey := c.prefix + "." + alias; viper.IsSet(prefixedKey) {
					return prefixedKey, true
				}
			}
		}
	}
	if prefixedKey := c.prefix + theRealKey; viper.IsSet(prefixedKey) {
		return prefixedKey, true
	}
	if !prefixHasDotEnd {
		if prefixedKey := c.prefix + "." + theRealKey; viper.IsSet(prefixedKey) {
			return prefixedKey, true
		}
	}

	// 2nd processing all aliases
	prefixAliases := c.GetPrefixAliases()
	for i := 0; i < len(prefixAliases); i++ {
		prefixAlias := prefixAliases[i]
		dotEnd := strings.HasPrefix(prefixAlias, ".")

		if aliasExist {
			for _, alias := range keyAliases {
				if prefixedKey := prefixAlias + alias; viper.IsSet(prefixedKey) {
					return prefixedKey, true
				}
				if !dotEnd {
					if prefixedKey := prefixAlias + "." + alias; viper.IsSet(prefixedKey) {
						return prefixedKey, true
					}
				}
			}
		}

		if prefixedKey := prefixAlias + theRealKey; viper.IsSet(prefixedKey) {
			return prefixedKey, true
		}
		if !dotEnd {
			if prefixedKey := prefixAlias + "." + theRealKey; viper.IsSet(prefixedKey) {
				return prefixedKey, true
			}
		}
	}

	// 3rd processing base prefix
	prefixHasDotEnd = strings.HasPrefix(c.base.prefix, ".")
	if aliasExist {
		for _, alias := range keyAliases {
			if prefixedKey := c.base.prefix + alias; viper.IsSet(prefixedKey) {
				return prefixedKey, true
			}
			if !prefixHasDotEnd {
				if prefixedKey := c.base.prefix + "." + alias; viper.IsSet(prefixedKey) {
					return prefixedKey, true
				}
			}
		}
	}
	if prefixedKey := c.base.prefix + theRealKey; viper.IsSet(prefixedKey) {
		return prefixedKey, true
	}
	if !prefixHasDotEnd {
		if prefixedKey := c.base.prefix + "." + theRealKey; viper.IsSet(prefixedKey) {
			return prefixedKey, true
		}
	}

	return findKey(key)
}

func (c *Prefix) doOrDie(keyExist bool, key string) {
	doOrDie(keyExist, c.key(key))
}

// IsSet checks to see if the key/aliases has been set
func (c *Prefix) IsSet(key string) bool {
	_, keyExist := c.findKey(key)
	return keyExist
}

func (c *Prefix) GetBool(key string) bool {
	validKey, _ := c.findKey(key)
	return viper.GetBool(validKey)
}

func (c *Prefix) MustGetBool(key string) bool {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetBool(validKey)
}

func (c *Prefix) GetDuration(key string) time.Duration {
	validKey, _ := c.findKey(key)
	return viper.GetDuration(validKey)
}

func (c *Prefix) MustGetDuration(key string) time.Duration {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetDuration(validKey)
}

func (c *Prefix) GetFloat64(key string) float64 {
	validKey, _ := c.findKey(key)
	return viper.GetFloat64(validKey)
}

func (c *Prefix) MustGetFloat64(key string) float64 {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetFloat64(validKey)
}

func (c *Prefix) GetInt(key string) int {
	validKey, _ := c.findKey(key)
	return viper.GetInt(validKey)
}

func (c *Prefix) MustGetInt(key string) int {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetInt(validKey)
}

func (c *Prefix) GetInt32(key string) int32 {
	validKey, _ := c.findKey(key)
	return viper.GetInt32(validKey)
}

func (c *Prefix) MustGetInt32(key string) int32 {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetInt32(validKey)
}

func (c *Prefix) GetInt64(key string) int64 {
	validKey, _ := c.findKey(key)
	return viper.GetInt64(validKey)
}

func (c *Prefix) MustGetInt64(key string) int64 {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetInt64(validKey)
}

func (c *Prefix) GetIntSlice(key string) []int {
	validKey, _ := c.findKey(key)
	return viper.GetIntSlice(validKey)
}

func (c *Prefix) MustGetIntSlice(key string) []int {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetIntSlice(validKey)
}

func (c *Prefix) GetSizeInBytes(key string) uint {
	validKey, _ := c.findKey(key)
	return viper.GetSizeInBytes(validKey)
}

func (c *Prefix) MustGetSizeInBytes(key string) uint {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetSizeInBytes(validKey)
}

func (c *Prefix) GetString(key string) string {
	validKey, _ := c.findKey(key)
	return viper.GetString(validKey)
}

func (c *Prefix) MustGetString(key string) string {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetString(validKey)
}

func (c *Prefix) GetStringMap(key string) map[string]interface{} {
	validKey, _ := c.findKey(key)
	return viper.GetStringMap(validKey)
}

func (c *Prefix) MustGetStringMap(key string) map[string]interface{} {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetStringMap(validKey)
}

func (c *Prefix) GetStringMapString(key string) map[string]string {
	validKey, _ := c.findKey(key)
	return viper.GetStringMapString(validKey)
}

func (c *Prefix) MustGetStringMapString(key string) map[string]string {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetStringMapString(validKey)
}

func (c *Prefix) GetStringMapStringSlice(key string) map[string][]string {
	validKey, _ := c.findKey(key)
	return viper.GetStringMapStringSlice(validKey)
}

func (c *Prefix) MustGetStringMapStringSlice(key string) map[string][]string {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetStringMapStringSlice(validKey)
}

func (c *Prefix) GetStringSlice(key string) []string {
	validKey, _ := c.findKey(key)
	return viper.GetStringSlice(validKey)
}

func (c *Prefix) MustGetStringSlice(key string) []string {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetStringSlice(validKey)
}

func (c *Prefix) GetTime(key string) time.Time {
	validKey, _ := c.findKey(key)
	return viper.GetTime(validKey)
}

func (c *Prefix) MustGetTime(key string) time.Time {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetTime(validKey)
}

func (c *Prefix) GetUint(key string) uint {
	validKey, _ := c.findKey(key)
	return viper.GetUint(validKey)
}

func (c *Prefix) MustGetUint(key string) uint {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetUint(validKey)
}

func (c *Prefix) GetUint32(key string) uint32 {
	validKey, _ := c.findKey(key)
	return viper.GetUint32(validKey)
}

func (c *Prefix) MustGetUint32(key string) uint32 {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetUint32(validKey)
}

func (c *Prefix) GetUint64(key string) uint64 {
	validKey, _ := c.findKey(key)
	return viper.GetUint64(validKey)
}

func (c *Prefix) MustGetUint64(key string) uint64 {
	validKey, keyExist := c.findKey(key)
	c.doOrDie(keyExist, key)
	return viper.GetUint64(validKey)
}
