# Config tools

Fungsi pengembangan dari [viper](https://github.com/spf13/viper) dengan pengambahan fungsi-fungsi berikut:
* key alias
* pengecekan value

## Get

mengambil nilai dari suati key tertentu. tersedia fungsi-fungsi berikut:
* GetString
* GetBool
* GetInt
* GetInt32
* GetInt64
* GetUint
* GetUint32
* GetUint64
* GetFloat64
* GetDuration
* GetTime
* GetStringSlice
* GetIntSlice

## Default value & aliasing

```go
cfgUtil.SetDefault("db.connect.timeout", 5, "db.connect-timeout", "db.timeout")
```
pada contoh diatas, key `db.connect.timeout` berlaku ketentuan sebagai berikut:
* di-set dengan nilai default `5`
* memiliki alias `db.connect-timeout` dan `db.timeout`

dengan demikian fungsi `Get` berikut akan memberikan nilai kembalian yang sama

```go
cfgUtil.GetDuration("db.connect.timeout")

cfgUtil.GetDuration("db.connect-timeout")

cfgUtil.GetDuration("db.timeout")
```

## MustGet

Sama seperti fungsi-fungsi `Get`, perbedaannya adalah apabila key yang dimaksud tidak memiliki nilai maka akan dianggap sebagai fatal error. Fungsi yang dipanggil pada saat terjadi fatal error bisa di set dengan `SetFatalHandler`.


---
[Back](../README.md)
