package config_test

import (
	"bytes"
	"fmt"
	"testing"

	"github.com/spf13/viper"
	cfgUtil "gitlab.com/watonist/letsgo/config"
)

func expectBoolP(t *testing.T, c *cfgUtil.Prefix, key string, expected bool) {
	if result := c.GetBool(key); result != expected {
		t.Errorf("Prefix:'%s' Key:'%s' Result:%v Expected:%v\n", c.GetPrefix(), key, result, expected)
	}
}

func expectIntP(t *testing.T, c *cfgUtil.Prefix, key string, expected int) {
	if result := c.GetInt(key); result != expected {
		t.Errorf("Prefix:'%s' Key:'%s' Result:%v Expected:%v\n", c.GetPrefix(), key, result, expected)
	}
}

func expectStringP(t *testing.T, c *cfgUtil.Prefix, key string, expected string) {
	if result := c.GetString(key); result != expected {
		t.Errorf("Prefix:'%s' Key:'%s' Result:%v Expected:%v\n", c.GetPrefix(), key, result, expected)
	}
}

func TestPrefixInit(t *testing.T) {
	cfg1, err := cfgUtil.NewPrefix("cfg1")
	if err != nil {
		t.Errorf("Error: %v\n", err)
	}
	cfg2, err := cfgUtil.NewPrefix("cfg2")
	if err != nil {
		t.Errorf("Error: %v\n", err)
	}
	if err := cfg1.SetPrefixAlias("c1", "config1", "c-1"); err != nil {
		t.Errorf("Error: %v\n", err)
	}
	if err := cfg2.SetPrefixAlias("c2", "c-2"); err != nil {
		t.Errorf("Error: %v\n", err)
	}
	fmt.Println("Prefix:", cfg1.GetPrefix())
	fmt.Println("Alias:", cfg1.GetPrefixAliases())

	fmt.Println("Prefix:", cfg2.GetPrefix())
	fmt.Println("Alias:", cfg2.GetPrefixAliases())

	configC1, err := cfgUtil.NewPrefix("c1")
	if err != nil {
		t.Errorf("Error: %v\n", err)
	}
	fmt.Println("Prefix:", configC1.GetPrefix())
	fmt.Println("Alias:", configC1.GetPrefixAliases())
}

func TestPrefixedAliases(t *testing.T) {
	rawConfig := []byte(`
mysql:
  host: 10.0.0.1
  port: 3306
  user: baseuser

home:
  mysql:
    host: 172.1.1.1

local:
  mysql:
    host: 127.0.0.1
    port: 3307
`)
	viper.SetConfigType("YAML")
	viper.ReadConfig(bytes.NewBuffer(rawConfig))

	cfgDefault, err := cfgUtil.NewPrefix("default")
	if err != nil {
		t.Errorf("Error: %v\n", err)
	}

	cfgHome, err := cfgUtil.NewPrefix("home")
	if err != nil {
		t.Errorf("Error: %v\n", err)
	}
	if err := cfgHome.SetPrefixAlias("local"); err != nil {
		t.Errorf("Error: %v\n", err)
	}

	expectString(t, "mysql.host", "10.0.0.1")
	expectInt(t, "mysql.port", 3306)
	expectString(t, "mysql.user", "baseuser")

	expectStringP(t, cfgDefault, "mysql.host", "10.0.0.1")
	expectIntP(t, cfgDefault, "mysql.port", 3306)
	expectStringP(t, cfgDefault, "mysql.user", "baseuser")

	expectStringP(t, cfgHome, "mysql.host", "172.1.1.1")
	expectIntP(t, cfgHome, "mysql.port", 3307)
	expectStringP(t, cfgHome, "mysql.user", "baseuser")

	cfgLocal, err := cfgUtil.NewPrefix("local")
	if err != nil {
		t.Errorf("Error: %v\n", err)
	}

	expectStringP(t, cfgLocal, "mysql.host", "127.0.0.1")
	expectIntP(t, cfgLocal, "mysql.port", 3307)
	expectStringP(t, cfgLocal, "mysql.user", "baseuser")
}
